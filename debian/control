Source: astromatic
Section: science
Priority: optional
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.4.1
Homepage: https://www.astromatic.net/software/
Vcs-Git: https://salsa.debian.org/debian-astro-team/astromatic.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/astromatic

Package: astromatic
Architecture: all
Depends: ${misc:Depends}
Recommends: missfits,
            psfex,
            scamp,
            source-exctractor | sextractor,
            stiff,
            swarp,
            weightwatcher
Description: Astronomical pipeline software collection
 AstrOmatic software is meant to be run in batch mode on large quantities of
 data, mostly on Unix platforms, with minimum human intervention. It is
 currently in use in various image survey pipelines including TERAPIX, the
 Dark-Energy Survey Data Management system, and Astro-WISE.
 .
 This metapackage will install all software from AstrOmatic.net that is
 packaged for Debian.
